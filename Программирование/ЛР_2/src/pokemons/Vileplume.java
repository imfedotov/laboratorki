package src.pokemons;

import ru.ifmo.se.pokemon.*;
import src.moves.*;

public class Vileplume extends Pokemon {
	public Vileplume() {
		super();
		setData();
	}

	public Vileplume(String name, int level) {
		super(name, level);
		setData();
	}

	private void setData() {
		super.setStats(75, 80, 85, 110, 90, 50);
		super.setMove(new Confide(), new Swagger(), new Growth(), new Venoshock());
	}
}