package src.pokemons;

import ru.ifmo.se.pokemon.*;
import src.moves.*;

public class Xurkitree extends Pokemon {
	public Xurkitree() {
		super();
		setData();
	}

	public Xurkitree(String name, int level) {
		super(name, level);
		setData();
	}

	private void setData() {
		super.setStats(83, 89, 71, 173, 71, 83);
		super.setMove(new Swagger(), new Blizzard(), new Lunge(), new Leer());
	}
}