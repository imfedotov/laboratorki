package src.pokemons;

import ru.ifmo.se.pokemon.*;
import src.moves.*;

public class Gloom extends Pokemon {
	public Gloom() {
		super();
		setData();
	}

	public Gloom(String name, int level) {
		super(name, level);
		setData();
	}

	private void setData() {
		super.setStats(60, 65, 70, 85, 75, 40);
		super.setMove(new Confide(), new Swagger(), new Growth());
	}
}