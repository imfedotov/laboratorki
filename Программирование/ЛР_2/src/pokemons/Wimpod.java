package src.pokemons;

import ru.ifmo.se.pokemon.*;
import src.moves.*;

public class Wimpod extends Pokemon {
	public Wimpod() {
		super();
		setData();
	}

	public Wimpod(String name, int level) {
		super(name, level);
		setData();
	}

	private void setData() {
		super.setStats(25, 35, 40, 20, 30, 80);
		super.setMove(new DoubleTeam(), new Swagger(), new SandAttack());
	}
}