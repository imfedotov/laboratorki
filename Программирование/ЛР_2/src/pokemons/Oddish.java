package src.pokemons;

import ru.ifmo.se.pokemon.*;
import src.moves.*;

public class Oddish extends Pokemon {
	public Oddish() {
		super();
		setData();
	}

	public Oddish(String name, int level) {
		super(name, level);
		setData();
	}

	private void setData() {
		super.setStats(45, 50, 55, 75, 65, 30);
		super.setMove(new Confide(), new Swagger());
	}
}
