package src.pokemons;

import ru.ifmo.se.pokemon.*;
import src.moves.*;

public class Golisopod extends Pokemon {
	public Golisopod() {
		super();
		setData();
	}

	public Golisopod(String name, int level) {
		super(name, level);
		setData();
	}

	private void setData() {
		super.setStats(75, 125, 140, 60, 90, 40);
		super.setMove(new DoubleTeam(), new Swagger(), new SandAttack(), new RockSmash());
	}
}