// Final
package src.moves;

import ru.ifmo.se.pokemon.*;

public class Lunge extends PhysicalMove {
	public Lunge() {
		super(Type.BUG, 80, 100.0 / 100);
	}

	@Override
	protected String describe() {
		return "uses Lunge";
	}

	/*
	Inflicts regular damage.  Lowers the target's Attack by one stage.
	effect_chance: 100
	ailment: none @ 0
	*/

	@Override
	protected void applyOppEffects(Pokemon def) {
		def.addEffect(new Effect().turns(-1).stat(Stat.ATTACK, -1));
	}
}
