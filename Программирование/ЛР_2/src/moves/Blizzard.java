// Final
package src.moves;

import ru.ifmo.se.pokemon.*;

public class Blizzard extends SpecialMove {
	public Blizzard() {
		super(Type.ICE, 110, 70.0/ 100);
	}

	@Override
	protected String describe() {
		return "uses Blizzard";
	}

	/*
	Inflicts regular damage.  Has a $effect_chance% chance to freeze the target.

During hail, this move has 100% accuracy.  It also has a (100 - accuracy)% chance to break through the protection of protect and detect.
	effect_chance: 10
	ailment: freeze @ 10
	*/

	@Override
	protected void applyOppEffects(Pokemon opp) {
		if (new Effect().chance(10.0/100).success())
			Effect.freeze(opp);
	}
}
