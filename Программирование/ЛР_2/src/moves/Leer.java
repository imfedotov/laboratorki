// Final
package src.moves;

import ru.ifmo.se.pokemon.*;

public class Leer extends StatusMove {
	public Leer() {
		super(Type.NORMAL, 0, 100.0 / 100);
	}

	@Override
	protected String describe() {
		return "uses Leer";
	}

	/*
	Lowers the target's Defense by one stage.
	effect_chance: 100
	ailment: none @ 0
	*/
	@Override
	protected void applyOppEffects(Pokemon def) {
		def.addEffect(new Effect().turns(-1).stat(Stat.DEFENSE, -1));
	}
}
