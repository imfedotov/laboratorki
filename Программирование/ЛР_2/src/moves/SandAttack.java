// Final
package src.moves;

import ru.ifmo.se.pokemon.*;

public class SandAttack extends StatusMove {
	public SandAttack() {
		super(Type.GROUND, 0, 100.0 / 100);
	}

	@Override
	protected String describe() {
		return "uses Sand Attack";
	}

	/*
	Lowers the target's accuracy by one stage.
	effect_chance: None
	ailment: none @ 0
	*/

	@Override
	protected void applyOppEffects(Pokemon def) {
		def.addEffect(new Effect().turns(-1).stat(Stat.DEFENSE, -1));
	}
}
