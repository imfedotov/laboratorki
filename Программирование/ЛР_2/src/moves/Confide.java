// Final
package src.moves;

import ru.ifmo.se.pokemon.*;

public class Confide extends StatusMove {
	public Confide() {
		super(Type.NORMAL, 0, 0 / 100, 0, 0);
	}

	@Override
	protected String describe() {
		return "uses Confide";
	}

	/*
	Lowers the target's Special Attack by one stage.
	effect_chance: 100
	ailment: none @ 0
	*/

	@Override
	protected void applyOppEffects(Pokemon opp) {
		opp.addEffect(new Effect().turns(-1).stat(Stat.SPECIAL_ATTACK, -1));
	}
}
