// Final
package src.moves;

import ru.ifmo.se.pokemon.*;

public class RockSmash extends PhysicalMove {
	public RockSmash() {
		super(Type.FIGHTING, 40, 100.0 / 100);
	}

	@Override
	protected String describe() {
		return "uses Rock Smash";
	}

	/*
	Inflicts regular damage.  Has a $effect_chance% chance to lower the target's Defense by one stage.
	effect_chance: 50
	ailment: none @ 0
	*/

	@Override
	protected void applyOppEffects(Pokemon def) {
		def.addEffect(new Effect().chance(50.0 / 100).turns(-1).stat(Stat.DEFENSE, -1));
	}
}
