// Final
package src.moves;

import ru.ifmo.se.pokemon.*;

public class Swagger extends StatusMove {
	public Swagger() {
		super(Type.NORMAL, 0, 85.0 / 100);
	}

	@Override
	protected String describe() {
		return "uses Swagger";
	}

	/*
	Raises the target's Attack by two stages, then confuses it.  If the target's Attack cannot be raised by two stages, the confusion is not applied.
	effect_chance: None
	ailment: confusion @ 0
	*/
	@Override
	protected void applyOppEffects(Pokemon def) {
		int stages = 0;
		double prevStat = def.getStat(Stat.ATTACK);
		for (int i = 0; i < 2; i++) {
			def.addEffect(new Effect().turns(-1).stat(Stat.ATTACK, 1));
			stages += (prevStat != def.getStat(Stat.ATTACK)) ? 1 : 0;
			prevStat = def.getStat(Stat.ATTACK);
		}
		if (stages == 2)
			def.confuse();
	}
}
