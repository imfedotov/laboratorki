// Final
package src.moves;

import ru.ifmo.se.pokemon.*;

public class Venoshock extends SpecialMove {
	public Venoshock() {
		super(Type.POISON, 65, 100 / 100);
	}

	@Override
	protected String describe() {
		return "uses Venoshock";
	}

	/*
	Inflicts regular damage.  If the target is poisoned, this move has double power.
	effect_chance: None
	ailment: none @ 0
	*/

	@Override
	protected double calcBaseDamage(Pokemon att, Pokemon def) {
		double normalPower = this.power;

		if (def.getCondition() == Status.POISON)
			this.power = normalPower * 2;

		double result = super.calcBaseDamage(att, def);
		this.power = normalPower;
		return result;
	}
}
