// Final
package src.moves;

import ru.ifmo.se.pokemon.*;

public class Growth extends StatusMove {
	public Growth() {
		super(Type.NORMAL, 0, 100.0 / 100);
	}

	@Override
	protected String describe() {
		return "uses Growth";
	}

	/*
	Raises the user's Attack and Special Attack by one stage each.  During sunny day, raises both stats by two stages.
	effect_chance: None
	ailment: none @ 0
	*/
	@Override 
	protected void applySelfEffects(Pokemon att) {
		att.addEffect(new Effect().turns(-1).stat(Stat.ATTACK, 1).stat(Stat.SPECIAL_ATTACK, 1));
	}
}

