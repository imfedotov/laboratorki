// Final
package src.moves;

import ru.ifmo.se.pokemon.*;

public class DoubleTeam extends StatusMove {
	public DoubleTeam() {
		super(Type.NORMAL, 0, 0 / 100);
	}

	@Override
	protected String describe() {
		return "uses Double Team";
	}

	/*
	Raises the user's evasion by one stage.
	effect_chance: None
	ailment: none @ 0
	*/
	@Override 
	protected void applySelfEffects(Pokemon att) {
		att.addEffect(new Effect().turns(-1).stat(Stat.EVASION, 1));
	}
}
