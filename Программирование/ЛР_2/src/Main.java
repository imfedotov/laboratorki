package src;

import ru.ifmo.se.pokemon.*;

public class Main {
	public static void main(String[] args) {
		Battle b = new Battle();
		b.addAlly(new src.pokemons.Xurkitree("Treexurki", 55));
		b.addAlly(new src.pokemons.Vileplume("Plumevile", 32));
		b.addAlly(new src.pokemons.Wimpod("Wimpy Kid", 14));
		b.addFoe(new src.pokemons.Golisopod("Podgoliso", 15));
		b.addFoe(new src.pokemons.Oddish("Radish", 12));
		b.addFoe(new src.pokemons.Gloom("ADCKuu_TAIII,EP", 95));
		b.go();
	}
}
