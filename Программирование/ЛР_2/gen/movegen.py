import sys
import os.path
from pprint import pprint

import requests
from dataclasses import dataclass
import jinja2


API = "https://pokeapi.co/api/v2"

DIR = os.path.dirname(__file__)
MOVES_LIST = os.path.join(DIR, "moves.txt")
MOVES_OUTPATH = "src/moves"
TEMPLATE = "move.javat"


env = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath=DIR))
env.filters["camelupper"] = lambda s: s.title().replace(" ", "")
env.filters["en"] = lambda l: [x for x in l if x["language"] == "en"][0]["name"]


def filter_url(obj):
	if isinstance(obj, dict):
		if "url" in obj and len(obj) > 1:
			key = list(obj.keys() - {"url"})[0]
			return obj[key]
		else:
			for k, v in obj.items():
				obj[k] = filter_url(v)
	if isinstance(obj, list):
		for i in range(len(obj)):
			obj[i] = filter_url(obj[i])
	return obj


def get_move_data(name: str):
	api_name = name.lower().replace(' ', '-')
	r = requests.get(f"{API}/move/{api_name}")
	data = r.json()
	return data


def render_move(move: dict) -> str:
	template = env.get_template(TEMPLATE)

	return template.render(**move)


if __name__ == "__main__":
	with open(MOVES_LIST, "r") as f:
		moves = f.read().splitlines()

	for mv in moves:
		print("move", mv)
		data = get_move_data(mv)

		data = filter_url(data)
		#pprint(data)
		
		with open(f"{MOVES_OUTPATH}/"+env.filters["camelupper"](mv)+".java", "w") as f:
			f.write(render_move(data))

