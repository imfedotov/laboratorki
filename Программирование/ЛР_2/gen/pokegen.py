import sys
import os.path
from pprint import pprint

import requests
from dataclasses import dataclass
import jinja2


API = "https://pokeapi.co/api/v2"

DIR = os.path.dirname(__file__)
POKEMONS_LIST = os.path.join(DIR, "pokemons.txt")
POKEMONS_OUTPATH = "src/pokemons"
TEMPLATE = "pokemon.javat"


env = jinja2.Environment(loader=jinja2.FileSystemLoader(searchpath=DIR))
env.filters["camelupper"] = lambda s: s.title().replace(" ", "")
env.filters["en"] = lambda l: [x for x in l if x["language"] == "en"][0]["name"]
env.filters["stat"] = lambda nm: (lambda l: [x for x in l if x["name"] == nm][0]["base_stat"])

def filter_url(obj):
	if isinstance(obj, dict):
		if "url" in obj and len(obj) > 1:
			key = list(obj.keys() - {"url"})[0]
			return obj[key]
		else:
			for k, v in obj.items():
				obj[k] = filter_url(v)
	if isinstance(obj, list):
		for i in range(len(obj)):
			obj[i] = filter_url(obj[i])
	return obj


def get_poke_data(name: str):
	api_name = name.lower().replace(' ', '-')
	r = requests.get(f"{API}/pokemon/{api_name}")
	data = r.json()
	return data


def render_move(move: dict) -> str:
	template = env.get_template(TEMPLATE)

	return template.render(**move)


if __name__ == "__main__":
	with open(POKEMONS_LIST, "r") as f:
		poke = []
		for l in f:
			name, *moves = l.strip().split(":")
			poke.append((name, moves))

	for name, moves in poke:
		print("poke", name, "moves", moves)
		data = filter_url(get_poke_data(name))
		data["moves"] = moves
		
		with open(f"{POKEMONS_OUTPATH}/"+env.filters["camelupper"](name)+".java", "w") as f:
			f.write(render_move(data))

