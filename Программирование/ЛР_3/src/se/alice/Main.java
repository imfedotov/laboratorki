package se.alice;

import se.alice.entities.*;
import se.alice.logging.Log;
import se.alice.logging.StdoutLogger;

public class Main {
    public static void main(String... args) {
        Log.setup(new StdoutLogger());

        Place pHouse = new Place("House");
        Place pStaircase = new Place("Staircase");

        pHouse.addSubplace(pStaircase);  // Would be nice if this added recursively
 
        IntelligentSubject sWhiteRabbit = new IntelligentSubject("White Rabbit", Subject.Sex.MALE); 
        sWhiteRabbit.new Intellect() {};

        IntelligentSubject sAlice = new IntelligentSubject("Alice", Subject.Sex.FEMALE);
        sAlice.new Intellect() { // Or use assignment?
            @Override
            public void reactTo(Entity e) {
                // So, this means that fearing white rabbit is part of her Intellect, so it must be hard coded [1]
                if (e instanceof IntelligentSubject && e.getName() == "White Rabbit") {
                    // if (this.getOwner().getSize() / e.getSize() < 1000) {
                        this.getOwner().say("I am afraid of White Rabbit");
                        this.getOwner().say("I am shivering");
                        this.getOwner().move(1337);
                    // }
                    Log.log("Note", "She forgot that she is, perhaps, a thousand times bigger than White Rabbit");
                }
            }
        };

        sWhiteRabbit.goTo(pHouse);  // Would be nice if rabbit is aut. added to pHouse.

        sAlice.goTo(pHouse);

        sWhiteRabbit.goTo(pStaircase);  // Would be nice if rabbit is aut. added to pHouse.

        sAlice.listenFor(sWhiteRabbit);  // [1] even if we are afraid we still have to listen
                                         // or be surprised if we don't
        
        sWhiteRabbit.move("light stomping");
    }
}
