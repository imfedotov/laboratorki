package se.alice.entities;

import se.alice.events.Event;
import se.alice.logging.Log;

// This is a class that represents an intelligent entity.
// So, an animal (like a mouse) cannot have this class, use plain Subject instead.
public class IntelligentSubject extends Subject {
    private Intellect intellect;
    
    @Override
    public String toString() {
        return "IntelligentSubject [name=" + name + " sex=" + sex + "]";
    }
    
    public abstract class Intellect {
        private IntelligentSubject owner;
        public IntelligentSubject getOwner() {
            return this.owner;
        }
        public Intellect() { 
            // This looks like a memory leak LMAO
            this.owner = IntelligentSubject.this;  
            IntelligentSubject.this.intellect = this;
        }  // default constructor?
        public void reactTo(Entity e) {}

        public void receiveEvent(Event e) {
            this.reactTo((Entity) e.getSource());
        }
    }

    public IntelligentSubject(String name, Sex sex) {
        super(name, sex);
    }

    public void say(String message) {
        Log.log(this.getClass().getSimpleName() + "," + this.name, message);
    }

    public Intellect getIntellect() {
        return intellect;
    }

    public void receiveEvent(Event e) {
        intellect.receiveEvent(e);
    }

    @Override
    public void goTo(Place p) {
        this.say("I am going to " + p.getName());
        super.goTo(p);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((intellect == null) ? 0 : intellect.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        IntelligentSubject other = (IntelligentSubject) obj;
        if (intellect == null) {
            if (other.intellect != null)
                return false;
        } else if (!intellect.equals(other.intellect))
            return false;
        return true;
    }

}
