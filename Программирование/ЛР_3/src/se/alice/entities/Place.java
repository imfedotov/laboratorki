package se.alice.entities;

import java.util.HashSet;
import java.util.Set;

import se.alice.events.EventsDispatcher;
import se.alice.logging.Log;

public final class Place extends Objekt {
    private Set<Entity> entities;

    public Place(String name) {
        this.name = name;
        this.entities = new HashSet<>();
    } 

    protected void addEntity(Entity e) {
        this.entities.add(e);
        if (e instanceof Subject)
            EventsDispatcher.getInstance().subscribe(this, (Subject) e);
        Log.log("Place," + this.name, String.format("%s entered", e.name));
    }

    public void addSubplace(Place p) {
        this.entities.add(p);
    }

    public boolean contains(Entity e) {
        return entities.contains(e);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((entities == null) ? 0 : entities.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Place other = (Place) obj;
        if (entities == null) {
            if (other.entities != null)
                return false;
        } else if (!entities.equals(other.entities))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Place [name=" + name + "]";
    }
}
