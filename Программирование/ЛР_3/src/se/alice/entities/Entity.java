package se.alice.entities;

import se.alice.events.Event;
import se.alice.events.IEventful;

public abstract class Entity implements IEventful {
    protected String name;

    public String getName() {
        return this.name;
    }

    public final Event presence() {
        return new Event(this, null);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Entity other = (Entity) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Entity [name=" + name + "]";
    }
}
