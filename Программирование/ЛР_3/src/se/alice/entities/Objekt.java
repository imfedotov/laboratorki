package se.alice.entities;

import se.alice.events.ForceEvent;

public abstract class Objekt extends Entity {
    @Override
	public String toString() {
		return "Objekt [name= " + name + "]";
	}

	public String receiveForceEvent(ForceEvent e) {
        if (e.getIntensity() > 1000)
            return "rumble";
        return "";
    }
}
