package se.alice.entities;

import se.alice.events.EventsDispatcher;
import se.alice.events.IEventful;
import se.alice.events.SoundEvent;
import se.alice.events.ForceEvent;

// This is class with traits that all subjects posess
public class Subject extends Entity {
    public enum Sex {
        MALE, FEMALE
    };

    protected Sex sex;

    public Sex getSex() {
		return sex;
	}
    
	public Subject(String name, Sex sex) {
        this.name = name;
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Subject [name=" + name + " sex=" + sex + "]";
    }

    public void listenFor(IEventful eventful) {
        EventsDispatcher.getInstance().subscribe(this, eventful);
    }

    public void goTo(Place p) {
        p.addEntity(this);
    }

    public void move(String how) {
        EventsDispatcher.getInstance().pushEvent(new SoundEvent(this, how));
    }

    public void move(int intensity) {
        EventsDispatcher.getInstance().pushEvent(new ForceEvent(this, intensity));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((sex == null) ? 0 : sex.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Subject other = (Subject) obj;
        if (sex != other.sex)
            return false;
        return true;
    }
}
