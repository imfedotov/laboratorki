package se.alice.events;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.LinkedList;

import se.alice.entities.*;
import se.alice.logging.Log;

public final class EventsDispatcher {
    private static EventsDispatcher globalController;

    //  Producer -> [Consumers]
    private Map<IEventful, Queue<Entity>> subscibers;

    private EventsDispatcher() {
        this.subscibers = new HashMap<IEventful, Queue<Entity>>();
    }

    public static EventsDispatcher getInstance() {
        if (globalController == null)
            globalController = new EventsDispatcher();
        return globalController;
    }

    public void subscribe(Entity consumer, IEventful producer) {
        if (this.subscibers.get(producer) == null)
            this.subscibers.put(producer, new LinkedList<>());
        this.subscibers.get(producer).add(consumer);
    }

    public void pushEvent(Event e) {
        if (e instanceof SoundEvent) {
            Log.log("EventsDispatcher,sound", 
                    ((Subject) e.getSource()).getName() 
                    + " made sound "
                    + ((SoundEvent) e).getDescription()
            );
        }

        for (Entity consumer : this.subscibers.get(e.getSource())) {
            if (consumer instanceof IntelligentSubject)
                ((IntelligentSubject) consumer).receiveEvent(e);
            else if (consumer instanceof Objekt && e instanceof ForceEvent) {
                String result = ((Objekt) consumer).receiveForceEvent((ForceEvent) e);
                if (result == "") continue;
                Log.log("EventsDispatcher,force" ,
                        ((Entity) e.getSource()).getName() 
                        + " made " + consumer.getName() + " "
                        + result
                );
            }
        }
    }

    @Override
    public String toString() {
        return "EventsDispatcher []";
    }
}
