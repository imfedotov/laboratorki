package se.alice.events;

public final class SoundEvent extends Event {
    private String description;

	@Override
    public String toString() {
        return "SoundEvent [description=" + description + "]";
    }

    public SoundEvent(IEventful source, String description) {
        super(source, null);
        this.description = description;
    }

	public String getDescription() {
		return description;
	}

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((description == null) ? 0 : description.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        SoundEvent other = (SoundEvent) obj;
        if (description == null) {
            if (other.description != null)
                return false;
        } else if (!description.equals(other.description))
            return false;
        return true;
    }
}
