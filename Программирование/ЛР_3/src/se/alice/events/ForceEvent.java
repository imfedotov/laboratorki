package se.alice.events;

public final class ForceEvent extends Event {
    private int intensity;

	@Override
    public String toString() {
        return "ForceEvent [intensity=" + intensity + "]";
    }

	public int getIntensity() {
		return intensity;
	}

    public ForceEvent(IEventful source, int intensity) {
        super(source, null);
        this.intensity = intensity;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + intensity;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ForceEvent other = (ForceEvent) obj;
        if (intensity != other.intensity)
            return false;
        return true;
    }
}
