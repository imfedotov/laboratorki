package se.alice.events;

import se.alice.entities.Entity;

public class Event {
    protected IEventful source;
    protected Object content;

	// Functions like a signed container
    public Event(IEventful source, Object content) {
        this.source = source;
        this.content = content;
    }
    
    public IEventful getSource() {
		return source;
	}

	public Object getContent() {
		return content;
	}

    @Override
    public String toString() {
        return "Event [source=" + source + ", content=" + content + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((source == null) ? 0 : source.hashCode());
        result = prime * result + ((content == null) ? 0 : content.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Event other = (Event) obj;
        if (source == null) {
            if (other.source != null)
                return false;
        } else if (!source.equals(other.source))
            return false;
        if (content == null) {
            if (other.content != null)
                return false;
        } else if (!content.equals(other.content))
            return false;
        return true;
    }
}

