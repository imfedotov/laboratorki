package se.alice.logging;

// This logger just prints to stdout with a tag.
public final class StdoutLogger implements ILogger {

	@Override
	public String toString() {
		return "StdoutLogger []";
	}

	@Override
	public void say(String tag, String message) {
        if (tag == "") { // Anonymous logging would be discouraged though.
            System.out.println(message);
        } else {
            System.out.printf("[%s] %s\n", tag, message);
        }
	}

}

