package se.alice.logging;

public class Log {
    @Override
	public String toString() {
		return "Log [loggerInstance= " + loggerInstance + "]";
	}

	private static ILogger loggerInstance;

    private Log() {}  // Disallow instantiation

    public static void setup(ILogger logger) {
        loggerInstance = logger;
    }

    public static void log(String tag, String message) {
        checkNull();
        loggerInstance.say(tag, message);
    }

    private static void checkNull() {
        if (loggerInstance == null)
            throw new RuntimeException("no logger was set");
    }
}
