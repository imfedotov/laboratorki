package se.alice.logging;

public interface ILogger {
    public void say(String tag, String message);
}

