import re

SMILEY = r"\[<{P"


def count_smileys(text: str, rg) -> int:
    return len(re.findall(rg, text))


tests = [
    ("My name is Skyler White, yo [<{P", 1),
    ("My husband is Walter White, yo [>{P", 0),
    ("Uh huh [<{ P [<{P [<{P", 2),
    ("[ {{ [ {{{{[<[ <[ {<[<[<[<{P<<<[[[[<<  {<{<[{<    [<{P[[[<[<[  [ <", 2),
    ("I [AM<{ [<{P LABORATO{<[POMING", 1),
    (r"sdafg\*\\) safgj*\\) \* \\ )", 0)
]

for text, n in tests:
    # print(count_smileys(text))
    assert count_smileys(text, SMILEY) == n

if __name__ == "__main__":
    import sys
    smiley = sys.stdin.readline().strip()
    text = sys.stdin.read()
    
    print(count_smileys(text, SMILEY+"|"+re.escape(smiley)))

# vim:noai:nocin:nosi:inde=
