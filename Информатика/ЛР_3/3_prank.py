import re

rgx_t = r"^(\w)\w+(?:-(\w)\w+)?\s+(?:\1\.\s*\1\.|\2\.\s*\2\.)\s+{}\s?"


def prank(text, my_group):
    return re.sub(rgx_t.format(my_group), "", text, count=0, flags=re.U|re.M)


tests = [
    (("Петров П.П. P0000\nАнищенко А.А. P33113\nПримеров Е.В. P0000\nИванов И.И. P0000", "P0000"), 
     "Анищенко А.А. P33113\nПримеров Е.В. P0000"),
    (("Фаааа Ф.Ф. P3113\nРаааа Р.Р. P3113\nНаааа Н.Н. P0000\nРаааа Р.Р. P0000", "P3113"),
        "Наааа Н.Н. P0000\nРаааа Р.Р. P0000"),
    (("Фаааа Ф.Ф. P3113\nРаааа Р.Р. P3113\nНаааа Н.Н. P0000\nРаааа Р.Р. P0000", "P0000"),
     "Фаааа Ф.Ф. P3113\nРаааа Р.Р. P3113"),
]

for ((text, my_group), exp) in tests:
    res = prank(text, my_group).removesuffix("\n")
    # print(res)
    assert res == exp

if __name__ == "__main__":
    import sys
    my_group = sys.argv[1]
    text = open(0).read()

    print(prank(text, my_group))

