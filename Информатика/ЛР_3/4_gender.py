import re

rgx = r"(?i)\w+\s+\w+\s+\w+(ич|на)"


def is_male(name):  # 1 if Male 0 if Female
    return re.findall(rgx, name) == ["ич"] 


tests = [
    ("Зайцев Виталий Иванович", True),
    ("Прохоров Ким Федотович", True),
    ("Павлов Владимир Артемович", True),
    ("Носкова Адельфина Тихоновна", False),
    ("Лазарева Вера Натановна", False)
]

for name, exp in tests:
    assert is_male(name) == exp


if __name__ == "__main__":
    import sys
    name = sys.argv[1]
    print(is_male(name))
