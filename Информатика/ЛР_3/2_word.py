import re


def expand_rgx(rg):
    # абвгдеёжзийклмнопрстуфхцчшщъыьэюя
    CYR_VOWELS = "АЕЁИОУЫЭЮЯ"
    CYR_CONSONANT = "БВГДЖЗЙКЛМНПРСТФХЦЧШЩЪЬ"
    return rg.replace("[:cyr_v:]", CYR_VOWELS)\
             .replace("[:cyr_c:]", CYR_CONSONANT)


rgx = expand_rgx(r"(?i)(\w*[[:cyr_v:]]{2}\w*)(?=[.,]?\s+(?:[[:cyr_v:]]*[[:cyr_c:]]){1,3}[[:cyr_v:]]*[.,]?(?:$|\s+))")


def find_words(text):
    return re.findall(rgx, text, flags=re.U)


tests = [
    ("Кривошеее существо гуляет по парку", ["гуляет"]),
    ("Кривошеее существо. гуляет по, парку", ["гуляет"]),
    ("День, в который все это произошло, начался как обычно, если не считать, что Знайка, проснувшись, встал не сразу, а, вопреки своим правилам, разрешил себе немножко поваляться в постели.", []),
    ("Решив прибегнуть к этому способу, Знайка бодро вскочил с постели и принялся делать утреннюю зарядку. Проделав ряд гимнастических упражнений и умывшись холодной водой", []),
    ("Это давно надо было бы сделать, да у Знайки все как-то времени не хватало.", []),
    ("Особенно теперь, коща у них в доме были гости.", []),
]

for (text, exp) in tests:
    # print(find_words(text))
    assert find_words(text) == exp

if __name__ == "__main__":
    text = open(0).read()
    print(*find_words(text), sep="\n")

