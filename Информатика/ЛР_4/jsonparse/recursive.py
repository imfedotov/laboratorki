from __future__ import annotations
from collections.abc import Iterable
from functools import wraps
from os import getenv
import sys
from typing import Any
from enum import IntEnum


def esc(code):
    return f"\033[{code}m"  # ]


def bg(col):
    return esc(f"48;5;{col}")


def fg(col):
    return esc(f"38;5;{col}")


def abstract(f):
    @wraps(f)
    def wrapper(obj, *args, **kwargs):
        typ = type(obj) if not isinstance(obj, type) else obj
        raise TypeError(
            "abstract method {}.{} not implemented".format(typ.__name__, f.__name__)
        )

    return wrapper


def logger():
    level = 0

    def make_wrapper(f):
        @wraps(f)
        def log_wrapper(self: _RuleBase, *args, **kwargs):
            nonlocal level

            if not isinstance(self, _RuleBase):
                r = f(self, *args, **kwargs)
                print(
                    fg(8) + "¦ " * level + esc(0),
                    f.__name__ + "()",
                    (" -> " + repr(r) if r is not None else ""),
                    sep="",
                    file=sys.stderr,
                )
                return r

            print(fg(8) + "¦ " * level + esc(0), self, "{", sep="", file=sys.stderr)
            level += 1
            r = f(self, *args, **kwargs)
            level -= 1
            print(
                fg(8) + "¦ " * level + esc(0),
                self,
                "} -> ",
                (fg(46) + str(r) + esc(0) if r else fg(1) + str(r) + esc(0)),
                sep="",
                file=sys.stderr,
            )
            return r

        return log_wrapper if getenv("DEBUG") else f

    return make_wrapper


log = logger()


class MyStringIO:
    _str: str
    _len: int
    _pos: int
    _prev_pos: list[int]

    def __init__(self, src: str):
        self._str = src
        self._len = len(src)
        self._pos = 0
        self._prev_pos = []

    @log
    def read(self, n: int):
        self._prev_pos.append(self._pos)
        s = self._str[self._pos : self._pos + n]
        self._pos += n
        # self.pos()
        return s

    def read_all(self):
        return self._str[self._pos :]

    @log
    def undo(self):
        if len(self._prev_pos) == 0:
            raise ValueError("can't backtrack")
        self.seek(self._prev_pos.pop())
        # self.pos()

    @log
    def forget(self, n: int):
        for _ in range(n):
            self._prev_pos.pop()
        # self.pos()

    def seek(self, pos: int):
        self._pos = pos

    def is_eof(self):
        return not self._pos < self._len

    @log
    def pos(self):
        return tuple(self._str[: pos + 1] for pos in self._prev_pos)

    def reset(self):
        self._pos = 0
        self._prev_pos = []


class AcceptResult(IntEnum):
    NO = 0
    AC = 1
    NA = 2

    def __str__(self):
        return [f"{fg(1)}{{}}{esc(0)}", f"{fg(2)}{{}}{esc(0)}", f"{fg(3)}{{}}{esc(0)}"][
            self.value
        ].format(self.name)


class OptNone:
    pass


class _RuleBase:
    @abstract
    def accept(self, stream: MyStringIO) -> AcceptResult:  # type: ignore
        pass

    @staticmethod
    @abstract
    def parse(stream: MyStringIO) -> Any:  # type: ignore
        return None


class _Chainable(_RuleBase):
    def __add__(self, rhs):
        return Seq(self, rhs)

    def __or__(self, rhs):
        return Alt(self, rhs)


class _ChainableOp(_Chainable):
    rules: list[_RuleBase]

    def __init__(self, lhs, rhs):
        if isinstance(lhs, type(self)):
            self.rules = lhs.rules.copy()
        else:
            self.rules = [lhs]
        if isinstance(rhs, type(self)):
            self.rules += rhs.rules
        else:
            self.rules.append(rhs)


def first(rules: Iterable[_RuleBase], stream: MyStringIO):
    for r in rules:
        if r.accept(stream):
            return r


class Alt(_ChainableOp):
    @log
    def accept(self, stream: MyStringIO):
        if first(self.rules, stream):
            return AcceptResult.AC
        return AcceptResult.NO

    @log
    def parse(self, stream: MyStringIO):
        if (r := first(self.rules, stream)) is not None:
            stream.undo()
            return r.parse(stream)

    def __repr__(self):
        return f"Alt{self.rules}"


def all_count(b: Iterable[_RuleBase], stream: MyStringIO):
    f, c = AcceptResult.AC, 0

    it = iter(b)
    for r in it:
        if (ac := r.accept(stream)) is not AcceptResult.NO:
            c += ac is not AcceptResult.NA
        else:
            f = AcceptResult.NO
            break

    return f, c


class Seq(_ChainableOp):
    @log
    def accept(self, stream: MyStringIO):
        res, cnt = all_count(self.rules, stream)
        if res is AcceptResult.NO:  # parse failed, backtrack all
            for _ in range(cnt):
                stream.undo()
        if res is AcceptResult.AC:
            stream.forget(cnt - 1)  # accept and leave one checkpoint
        return res

    @log
    def parse(self, stream: MyStringIO):
        if self.accept(stream):
            stream.undo()
            res = []
            for r in self.rules:
                try:
                    res.append(r.parse(stream))
                except TypeError:
                    print(r)
                    raise
            return res

    def __repr__(self):
        return f"Seq{self.rules}"


class _RuleMeta(_Chainable, type):
    def __repr__(cls):
        return cls.__name__


class Rule(_Chainable, metaclass=_RuleMeta):
    _rule_cached: _RuleBase | None = None

    @abstract
    def _rule():
        pass

    @classmethod
    def rule(cls) -> _RuleBase:  # type: ignore
        if cls._rule_cached is None:
            # print(f"cached {cls}!")
            cls._rule_cached = cls._rule()
        return cls._rule_cached  # type: ignore

    @classmethod
    @log
    def accept(cls, stream: MyStringIO):
        return cls.rule().accept(stream)

    @classmethod
    @log
    def _parse(cls, stream: MyStringIO):
        return cls.rule().parse(stream)

    @classmethod
    def parse(cls, stream: MyStringIO):
        return cls._parse(stream)


class Lit(Rule):
    _value: str

    def __init__(self, value):
        self._value = value
        self._len = len(value)

    @log
    def accept(self, stream: MyStringIO):
        if stream.is_eof():
            return AcceptResult.NO
        ac = stream.read(self._len) == self._value
        if not ac:
            stream.undo()
        return AcceptResult.AC if ac else AcceptResult.NO

    @log
    def parse(self, stream: MyStringIO):
        if self.accept(stream):
            return self._value

    def __repr__(self):
        return repr(self._value)


class Range(Rule):
    _range: tuple[int, int]

    def __init__(self, from_, to):
        if not from_ < to:
            raise ValueError("`from_` must be less than `to`")
        self._range = (from_, to)

    # @log
    def accept(self, stream: MyStringIO):
        if stream.is_eof():
            return 0
        ac = self._range[0] <= ord(stream.read(1)) <= self._range[1]
        if not ac:
            stream.undo()
        return AcceptResult.AC if ac else AcceptResult.NO

    @log
    def parse(self, stream: MyStringIO):
        if self.accept(stream):
            stream.undo()
            return stream.read(1)

    def __repr__(self):
        return "{{#0x{:04x}, #0x{:04x}}}".format(*self._range)


class Opt(Rule):
    rule: _RuleBase

    def __init__(self, r: _RuleBase):
        self.rule = r

    @log
    def accept(self, stream: MyStringIO):
        ac = self.rule.accept(stream)
        return AcceptResult.AC if ac else AcceptResult.NA

    @log
    def parse(self, stream: MyStringIO):
        if ac := self.accept(stream):
            if ac is AcceptResult.NA:
                return OptNone
            stream.undo()
            return self.rule.parse(stream)

    def __repr__(self):
        return f"Opt[{self.rule}]"


class Rep(Rule):
    rule: _RuleBase

    def __init__(self, r: _RuleBase):
        if type(r) == Opt:
            raise TypeError("Rep(Opt(...)) forbidden")
        self.rule = r

    @log
    def accept(self, stream: MyStringIO):
        count = 0
        while self.rule.accept(stream) is AcceptResult.AC:
            count += 1
        stream.forget(count - 1)  # should be guarded inside forget()

        return AcceptResult.AC if count else AcceptResult.NO

    @log
    def parse(self, stream: MyStringIO):
        res = []
        while self.rule.accept(stream):
            stream.undo()
            res.append(self.rule.parse(stream))

        if all(type(v) == str for v in res):
            return "".join(res)
        return res

    def __repr__(self):
        return f"Rep[{self.rule}]"


class ws(Rule):
    @staticmethod
    def _rule():
        return Opt(Rep(Lit(" ") | Lit("\t") | Lit("\r") | Lit("\n")))


class begin_array(Rule):
    @staticmethod
    def _rule():
        return ws + Lit("[") + ws  # ]


class begin_object(Rule):
    @staticmethod
    def _rule():
        return ws + Lit("{") + ws  # }


class end_array(Rule):
    @staticmethod
    def _rule():
        return ws + Lit("]") + ws


class end_object(Rule):
    @staticmethod
    def _rule():
        return ws + Lit("}") + ws


class name_separator(Rule):
    @staticmethod
    def _rule():
        return ws + Lit(":") + ws


class value_separator(Rule):
    @staticmethod
    def _rule():
        return ws + Lit(",") + ws


# Number
class DIGIT(Rule):
    @staticmethod
    def _rule():
        return Range(ord("0"), ord("9"))


class integer(Rule):
    @staticmethod
    def _rule():
        return Lit("0") | Range(ord("1"), ord("9")) + Opt(Rep(DIGIT))

    @classmethod
    def parse(cls, stream: MyStringIO):
        _integer = cls._parse(stream)
        if _integer == "0":
            return _integer

        digi1, digi = _integer
        return digi1 + ("" if digi is OptNone else digi)


class e(Rule):
    @staticmethod
    def _rule():
        return Lit("E") | Lit("e")


class exp(Rule):
    @staticmethod
    def _rule():
        return e + Opt(Lit("-") | Lit("+")) + Rep(DIGIT)

    @classmethod
    def parse(cls, stream: MyStringIO):
        _e, _sig, digi = cls._parse(stream)
        return _e + ("" if _sig is OptNone else _sig) + digi


class frac(Rule):
    @staticmethod
    def _rule():
        return Lit(".") + Rep(DIGIT)

    @classmethod
    def parse(cls, stream: MyStringIO):
        dot, digi = cls._parse(stream)
        return dot + digi


class number(Rule):
    @staticmethod
    def _rule():
        return Opt(Lit("-")) + integer + Opt(frac) + Opt(exp)

    @classmethod
    def parse(cls, stream: MyStringIO) -> float | int:
        parts = cls._parse(stream)
        return eval("".join(map(lambda p: "" if p is OptNone else p, parts)))  # yeah


# string
class hex(Rule):
    @staticmethod
    def _rule():
        return DIGIT | Range(ord("a"), ord("f")) | Range(ord("A"), ord("F"))


class escaped(Rule):
    @staticmethod
    def _rule():
        return Lit("\\") + (
            Lit('"')
            | Lit("\\")
            | Lit("/")
            | Lit("b")
            | Lit("f")
            | Lit("n")
            | Lit("r")
            | Lit("t")
            | Lit("u") + hex + hex + hex + hex
        )

    @classmethod
    def parse(cls, stream: MyStringIO):
        esc, chr = cls._parse(stream)
        return (esc + "".join(chr)).encode().decode("unicode-escape")


class unescaped(Rule):
    @staticmethod
    def _rule():
        return Range(0x20, 0x21) | Range(0x23, 0x5B) | Range(0x5D, 0x10FFFF)


class char(Rule):
    @staticmethod
    def _rule():
        return unescaped | escaped


class string(Rule):
    @staticmethod
    def _rule():
        return Lit('"') + Opt(Rep(char)) + Lit('"')

    @classmethod
    def parse(cls, stream: MyStringIO):
        _, d, _ = cls._parse(stream)
        return "" if d is OptNone else d


# value
class _special(Rule):
    val = None
    lit = ""

    @classmethod
    def _rule(cls):
        return Lit(cls.lit or cls.__name__)

    @classmethod
    def parse(cls, stream: MyStringIO):
        if cls.accept(stream):
            return cls.val


class true(_special):
    val = True


class false(_special):
    val = False


class null(_special):
    val = None


class value(Rule):
    @staticmethod
    def _rule():
        return true | false | null | objekt | array | number | string


# array
class array(Rule):
    @staticmethod
    def _rule():
        return begin_array + Opt(value + Opt(Rep(value_separator + value))) + end_array

    @classmethod
    def parse(cls, stream: MyStringIO):
        _, arr, _ = cls._parse(stream)
        if arr is OptNone:
            return []

        elem0, elems = arr
        if elems is OptNone:
            return [elem0]

        res = [elem0]
        for elem in elems:
            _, val = elem
            res.append(val)

        return res


# object
class member(Rule):
    @staticmethod
    def _rule():
        return string + name_separator + value

    @classmethod
    def parse(cls, stream: MyStringIO):
        k, _, v = cls._parse(stream)
        return (k, v)


class objekt(Rule):
    @staticmethod
    def _rule():
        return (
            begin_object + Opt(member + Opt(Rep(value_separator + member))) + end_object
        )

    @classmethod
    def parse(cls, stream: MyStringIO):
        _, obj, _ = cls._parse(stream)
        if obj is OptNone:
            return {}

        kv1, kv = obj
        if kv is OptNone:
            return dict([kv1])

        kvs = [kv1]
        for a in kv:
            _, x = a
            kvs.append(x)

        return dict(kvs)


class json(Rule):
    class ParseError(Exception):
        __module__ = Exception.__module__

    @staticmethod
    def _rule():
        return ws + value + ws

    @classmethod
    def accept(cls, stream: MyStringIO):
        ac = cls.rule().accept(stream)
        if not ac:
            raise json.ParseError("invalid json")
        if not stream.is_eof():
            raise json.ParseError("trailing characters")
        return ac

    @classmethod
    def parse(cls, stream: MyStringIO):
        if cls.accept(stream):
            stream.undo()
            _, d, _ = super(cls, cls)._parse(stream)
            return d


def parse_json(json_str: str) -> Any:
    stream = MyStringIO(json_str)
    return json.parse(stream)


def main():
    # print(value.rule())
    stream = MyStringIO(open(sys.argv[1]).read())
    print(json.parse(stream))


json.rule()  # trigger caching

if __name__ == "__main__":
    main()
