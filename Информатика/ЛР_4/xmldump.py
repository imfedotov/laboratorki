def dump_xml(data):
    if not isinstance(data, dict):
        raise TypeError("top level must be object")

    def _id(n):
        return "\n" + "\t" * n

    def _walk(obj, depth=0) -> str:
        if isinstance(obj, dict):
            kv_xml = []
            for key, val in obj.items():
                if any(c in "\x0d\x0a\x09\x20" for c in key):
                    raise ValueError(
                        "whitespace in key not allowed: {}".format(repr(key))
                    )

                kv_xml.append(
                    f'<{key} type="{type(val).__name__}">'
                    + _walk(val, depth=depth + 1)
                    + f"</{key}>"
                )
            return _id(depth) + _id(depth).join(kv_xml) + _id(depth - 1)

        elif isinstance(obj, list):
            vals_xml = map(lambda v: _walk(v, depth=depth + 1), obj)
            return "".join(vals_xml)

        else:
            return str(obj)  # Any other type becomes string

    return '<?xml version="1.0" encoding="UTF-8"?>' + _walk(data)
