import sys

from jsonparse.recursive import parse_json
from xmldump import dump_xml


def convert(json_s: str):
    data = parse_json(json_s)
    return dump_xml(data)


if __name__ == "__main__":
    with open(sys.argv[1]) as f:
        json_s = f.read()
        print(convert(json_s))
