import sys

from google.protobuf import json_format

import schedule_pb2
from jsonparse.recursive import parse_json


def convert(json_s):
    data = parse_json(json_str)

    day = schedule_pb2.Day()
    json_format.ParseDict(data, day)

    return day


if __name__ == "__main__":
    with open(sys.argv[1]) as f:
        json_str = f.read()
    print(convert(json_str))
