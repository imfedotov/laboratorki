import sys

from json2xml import json2xml
import json


def convert(json_s: str):
    data = json.loads(json_s)
    return json2xml.Json2xml(data).to_xml()


if __name__ == "__main__":
    with open(sys.argv[1]) as f:
        json_s = f.read()
        print(convert(json_s))
