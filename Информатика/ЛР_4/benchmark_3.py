from time import perf_counter

from task_0 import convert as convert0
from task_1 import convert as convert1


def run(f, times: int):
    start = perf_counter()
    for _ in range(times):
        f()
    end = perf_counter()
    dur = (end - start) * 1000
    return dur, dur / times


if __name__ == "__main__":
    import sys

    with open(sys.argv[1]) as f:
        json_str = f.read()

    for name, func in (
        ("my parser (0)", convert0),
        ("indian parser (1)", convert1),
    ):
        us, avg = run(lambda: func(json_str), 100)
        print(f"{name:>20}\ttotal {us}ms\tavg {avg:.06f}ms")
