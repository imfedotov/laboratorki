

def calc_syndrome(code: int) -> int:
    MASKS = [0b1010101, 0b0110011, 0b0001111]
    s = 0
    for m in reversed(MASKS):
        si = (code & m).bit_count() & 1
        s = (s << 1) | si
    return s


def correct(code: int) -> int:
    s = calc_syndrome(code)
    mask = (1 << (7 - s)) & 0b1111111
    code = (code ^ mask)

    return code


def get_data_bits(code: int) -> int:
    d = 0
    for i in range(1, 7+1):
        if i.bit_count() == 1:
            continue
        d = (d << 1) | ((code >> (7 - i)) & 1)
    return d


def decode(code: int) -> int:
    return get_data_bits(correct(code))


if __name__ == "__main__":
    import sys
    
    num = sys.argv[1]
    if len(num) != 7 or set(num) != set("01"):
        print("number must be 7-digit binary", file=sys.stderr)
        exit(1)

    data = decode(int(num, 2))
    print("{:04b}".format(data))

