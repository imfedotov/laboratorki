; BComp-NG Brainfuck interpreter 
;       with 32-bit integer cells.
; Author: Ivan Fedotov <imfedotov@niuitmo.ru>

ORG 20h
DEREF_20: WORD ?
start:	CLA
	OUT	0Ch
	CALL	bf_main		; noreturn


; proc noreturn bf_main(void)
bf_main:
.loop:		; debug output
		LD	bf.PC
		OUT	2
		LD	bf.TP
		OUT	6

		CALL	bf_readop
		CALL	bf_select
		ST	DEREF_20
		CALL	(DEREF_20)
		
		LD	(bf.PC)+
		BR	.loop
; bf_main endp

; proc __fastcall word* bf_select(byte)
bf_select:	CMP	#5Eh	; '^'
		BGE	.ret0
		CMP	#5Bh	; '['
		BLT	.0
		SUB	#5Ah	; 'Z'
		ADD	.jt.5b
		BR	.ret

.0:		CMP	#3Fh	; '?'
		BGE	.ret0
		CMP	#3Ch	; '<'
		BLT	.1
		SUB	#3Bh	; ';'
		ADD	.jt.3c
		BR	.ret

.1:		CMP	#2Fh	; '/'
		BGE	.ret0
		CMP	#2Bh	; '+'
		BLT	.ret0
		SUB	#2Ah	; '*'
		ADD	.jt.2b
		BR	.ret

.ret0:		LD	.jt.unk
.ret:		ST	DEREF_20
		LD	(DEREF_20)
		RET
.jt.2b:	WORD $.jt.2b
	WORD $bf_inc, $bf_input, $bf_dec, $bf_output
.jt.3c:	WORD $.jt.3c
	WORD $bf_left, $bf_stop, $bf_right
.jt.5b:	WORD $.jt.5b
	WORD $bf_jumpfwd, $bf_stop, $bf_jumpback
.jt.unk:  WORD $bf_stop
; bf_select endp

; proc __fastcall byte bf_readop(byte)
bf_readop:	LD	bf.PC
		ASR
		PUSHF
		ADD	bf.PRG
		ST	DEREF_20
		LD	(DEREF_20)
		POPF
		BCS	.ret
		SWAB
.ret:		AND	.mask
		RET
.mask: WORD 000FFh
; bf_readop endp


; INTERPRETER STATE
bf:
.PC:	WORD 0		; program counter
.SP:	WORD $stack	; stack
.TP:	WORD 0		; tape pointer/position

; current value
.V.1:	WORD 0
.V.0:	WORD 0

.PRG:	WORD $program	; code base pointer
.STKST:	 WORD $stack
.TAPEST: WORD $tape
; INTERPRETER STATE


; proc void bf_st(void)
bf_st:		PUSH
		LD	bf.TP
		ASL
		ADD	bf.TAPEST
		ST	DEREF_20
		LD	bf.V.1
		ST	(DEREF_20)+
		LD	bf.V.0
		ST	(DEREF_20)
		POP
		RET
; bf_st endp

; proc bf_ld(void)
bf_ld:		PUSH
		LD	bf.TP
		ASL
		ADD	bf.TAPEST
		ST	DEREF_20
		LD	(DEREF_20)+
		ST	bf.V.1
		LD	(DEREF_20)
		ST	bf.V.0
		POP
		RET
; bf_ld endp

; proc void bf_right(void)
bf_right:	PUSH
		CALL	bf_st
		LD	(bf.TP)+
		CALL	bf_ld
		POP
		RET
; bf_right endp

; proc void bf_left(void)
bf_left:	PUSH
		CALL	bf_st
		LD	-(bf.TP)
		CALL	bf_ld
		POP
		RET
; bf_left endp

; proc void bf_inc(void)
bf_inc:		PUSH
		LD	bf.V.0
		INC
		ST	bf.V.0
		BCC	.ret
		LD	(bf.V.1)+
.ret:		POP
		RET
; bf_inc endp
		
; proc void bf_dec(void)
bf_dec:		PUSH
		LD	bf.V.0
		DEC
		ST	bf.V.0
		BCS	.ret
		LD	-(bf.V.1)
.ret:		POP
		RET
; bf_dec endp

; proc void bf_output(void)
bf_output:	PUSH
.spinloop:	IN	0Dh
		AND	#40h
		BZS	.spinloop
		LD	bf.V.0
		OUT	0Ch
		POP
		RET
; bf_output endp

; proc void bf_input(void)
bf_input:	PUSH
.spinloop:	IN	19h
		AND	#40h
		BZS	.spinloop
		CLA
		ST	bf.V.1
		IN	18h
		ST	bf.V.0
		POP
		RET
; bf_input endp

; proc void bf_jumpfwd(void)
;	1: <ret>
;	0: cnt
bf_jumpfwd:	CLA
		PUSH
		LD	bf.V.0
		BZC	.push
		LD	bf.V.1
		BZC	.push

		LD	bf.STKST
		SUB	bf.SP
		ST	&0

.skip:		LD	(bf.PC)+
		CALL	bf_readop
		CMP	#5Dh	; ']'
		BNE	.skip
		LOOP	&0
		BR	.skip
		BR	.ret
.push:		LD	bf.PC	
		ST	-(bf.SP)
.ret:		POP
		RET
; bf_jumpfwd endp

; proc void bf_jumpback(void)
bf_jumpback:	PUSH
		LD	bf.V.0
		BZC	.goback
		LD	bf.V.1
		BZC	.goback
		LD	(bf.SP)+
		BR	.ret
.goback:	LD	(bf.SP)+
		DEC
		ST	bf.PC
.ret:		POP
		RET
; bf_jumpback endp

; proc noreturn bf_stop(void)
bf_stop:	HLT
; bf_stop endp


ORG 200h
; place your program code here
program:
hw1: WORD 2B2Bh, 2B2Bh, 2B2Bh, 2B2Bh, 5B3Eh, 2B2Bh, 2B2Bh, 5B3Eh, 2B2Bh, 3E2Bh, 2B2Bh, 3E2Bh, 2B2Bh, 3E2Bh, 3C3Ch, 3C3Ch, 2D5Dh, 3E2Bh, 3E2Bh, 3E2Dh, 3E3Eh, 2B5Bh, 3C5Dh, 3C2Dh, 5D3Eh, 3E2Eh, 3E2Dh, 2D2Dh, 2E2Bh, 2B2Bh, 2B2Bh, 2B2Bh, 2E2Eh, 2B2Bh, 2B2Eh, 3E3Eh, 2E3Ch, 2D2Eh, 3C2Eh, 2B2Bh, 2B2Eh, 2D2Dh, 2D2Dh, 2D2Dh, 2E2Dh, 2D2Dh, 2D2Dh, 2D2Dh, 2D2Eh, 3E3Eh, 2B2Eh, 3E2Bh, 2B2E

; cat: WORD 2C5Bh, 2E2Ch, 5D20h

WORD 0
tape: WORD 200h dup(0)

ORG 7F0h
stack: WORD 0
; vim:ts=8:sw=8:noet
